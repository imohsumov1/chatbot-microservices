package com.example.api.repos;

import com.example.api.entities.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChatRepo extends JpaRepository<Message, Long> {

    List<Message> findAllByConversation(Long id);

    List<Message> findAllByConversationAndUser(Long id, String user);
}

