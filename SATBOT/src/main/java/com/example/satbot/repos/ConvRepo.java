package com.example.satbot.repos;

import com.example.satbot.entities.Conversation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConvRepo extends JpaRepository<Conversation, Long> {
}

